
/-

  PART 3 - 1 : **Vectors**
  =================================

We now show a first, classical, example of the use of dependent
types to integrate computations and properties about such computations

a.k.a. **internal verification**

-/

-- This is (a possible definition of) the type of Vectors
-- or size-constrained lists.

inductive Vector (A : Type u) : Nat → Type u where
  | vnil  : Vector A 0
  | vcons : A → {n : Nat} → Vector A n → Vector A (n+1)
   deriving Repr  

open Vector
 
@[local simp]
def vex := vcons 1 (vcons 2 (vcons 3 (vcons 4 (vcons 5 vnil))))

#check vex

-- Basic operations

@[simp]
def Vector.head {A : Type} {n : Nat}: (v : Vector A (n + 1)) → A
  | vcons x _ => x

@[simp]
def Vector.tail {A : Type} {n : Nat}: (v : Vector A (n + 1)) → Vector A n
  | vcons _ v => v

#eval vex.head
#eval vex.tail.head
#eval vex.tail.tail.tail

-- Remark : the empty vector has no head nor tail
--#check vnil.head
--#check vnil.tail

-- Example : Concatenation of vectors

@[simp]
def vappend (v₁ : Vector A n₁) (v₂ : Vector A n₂) : Vector A (n₁ + n₂) :=
  match n₁, v₁ with
  | 0, vnil => by simp_arith ; exact v₂
  | n₁+1, vcons e v'₁ => let vres := vcons e (vappend v'₁ v₂)
                         by have Heq: n₁ + 1 + n₂ = n₁ + n₂ + 1 := by simp_arith
                            rw [Heq]
                            exact vres

#eval vappend vex vex

theorem vappend_vnil (v : Vector A n): vappend v vnil = v :=
by
  induction v
  case vnil => rfl
  case vcons e n v Hind =>
    simp [Hind]
    rfl

-- Petite question substidiaire :  quelle signature utiliserait-on pour :

-- - une fonction `vremove` qui enlève des éléments d'un vecteur
-- - une fonction `loadVector` qui charge un vecteur depuis un fichier disque ?

/-

  PART 1 - 1 :   an Appetizer ...
  ===============================

-/

-- Lean4 is first and foremost ...

--     ... a typed functional programming language

-- Related : ML (SML/Ocaml),  Haskell, Scala, ...

-- Example of a function :

@[simp]
def filter {α : Type} (pred? : α → Bool) (xs : List α) : List α :=
  match xs with
  | [] => []
  | y::ys => if pred? y then y :: filter pred? ys
             else filter pred? ys

#eval filter (fun x => x > 2) [1, 2, 3, 4, 5, 4, 3, 2, 1]

-- Et où sont les types dépendants ?

--   ... par exemple pour ce qui suit ...

-- 1) Express a (logical/mathematical) property about the program ...

theorem filterTrue {α : Type} (xs : List α):
  filter (fun _ => true) xs = xs :=

--  2) establish a proof that the program satisfies the property

by
  induction xs
  case nil => simp
  case cons x xs Hind =>
    simp [Hind]


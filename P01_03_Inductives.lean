
/-

  PART 1 - 3 : **inductives types**
  =================================
       (more precisely ... type families)

  ... or algebraic datatypes *on steroids*

-/

/- ----------------------------
  Example 1 : a **sum type**
----------------------------- -/

inductive Boolean : Type where
  | btrue : Boolean
  | bfalse : Boolean
  deriving Repr

open Boolean

#check btrue
#check bfalse

-- Function definitions with **pattern matching**
def bnot (b : Boolean) :=
  match b with
  | btrue => bfalse
  | bfalse => btrue

#eval bnot btrue
#eval bnot (bnot bfalse)

/- ---------------------------------------------
  Example 2 : a (polymorphic) **product type**
--------------------------------------------- -/
 
inductive Pair (α β : Type) : Type where
  | mk: α → β → Pair α β   
  deriving Repr

#check @Pair

#check Pair.mk 42 true

def first {α β : Type}: Pair α β → α
  | Pair.mk x _ => x

def second {α β : Type}: Pair α β → β
  | Pair.mk _ y => y

#eval first (Pair.mk 42 true)
#eval second (Pair.mk 42 true)

/- -----------------------------------------------
  Example 3 : a (polymorphic) **inductive type**
----------------------------------------------- -/

inductive BinTree (α : Type) where
  | tip: BinTree α
  | node (label : α) (left right : BinTree α)
  deriving Repr

open BinTree

-- Example :
def tree₁ := node 1 (node 2 (node 3 tip tip) (node 4 tip tip)) (node 5 tip tip) 
/- 

      1
     / \
    2   5
  / \
 3   4
           
-/

-- Function definition by **recursion**

@[simp]
def size (t : BinTree α) : Nat :=
  match t with
  | tip => 0
  | node _ l r => (size l) + (size r) + 1

#eval size tree₁

@[simp]
def mirror (t : BinTree α) : BinTree α :=
  match t with
  | tip => tip
  | node x l r => node x (mirror r) (mirror l)

-- Proofs by **induction**

theorem mirrorSize:
  ∀ (t : BinTree α), size (mirror t) = size t :=
by
  intro t
  induction t
  case tip => simp
  case node _ l r Hind₁ Hind₂ => 
    simp_arith [Hind₁, Hind₂]
    
-- Remark : this is (almost) the same as :

def mirrorSizeFun (t : BinTree α) :  size (mirror t) = size t :=
by
  induction t <;> simp_arith [*]  -- same proof, shorter

-- ... to confirm this :
#check @mirrorSize
#check @mirrorSizeFun

-- Hence :  a theorem is simply a (defined) function with
-- body of type `Prop`

#check (α : Type) → (t : BinTree α) → size (mirror t) = size t

/- ---------------------------------------
  Example 4 : an **inductive relations**
              (a type family)
--------------------------------------- -/

-- The property of "being a pair"
 inductive IsEven: Nat → Prop :=
  | zero: IsEven 0
  | succ_succ: (n : Nat) → (Hn: IsEven n) → IsEven (n + 2)

-- This can be described as the following *proof system* : 

/-
                          n : Nat    IsEven n
  ============= (zero)  ======================= (succ_succ)
     IsEven 0                 IsEven (n + 2)
-/

-- And this enables **rule induction**

theorem add_even (n : Nat):
  IsEven n → ∀ p : Nat, IsEven p → IsEven (n + p) :=
by
  intros Hn p
  induction Hn
  case zero => intro Hp
               simp
               assumption
  case succ_succ m _ Hind =>
    intros Hp
    have Hcomm: m + 2 + p = m + p + 2 := by simp_arith 
    rw[Hcomm]
    constructor
    apply Hind
    assumption

-- **Remark** : structural induction does not work here

import Lake
open Lake DSL

require std from git
  "https://github.com/leanprover/std4/" @ "3156cd5b375d1a932d590c918b7ad50e3be11947"

package «APR_Tutorial_Lean4» {
  -- add package configuration options here
}

lean_lib «APRTutorialLean4» {
  -- add library configuration options here
}

@[default_target]
lean_exe «APR_Tutorial_Lean4» {
  root := `Main
}
